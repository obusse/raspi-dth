# Building a Raspberry Pi Thermometer

## Important

Depending on your Raspberry Pi model this tutorial may vary. I built a thermometer based on a Pi 2 model B without built-in wifi using a dongle. This is important when it comes to choosing the correct Node.js package. In my case it's the ARM6-compliant package I installed manually.

You will also need the DHT22 sensor which you can find here: [https://www.amazon.de/gp/product/B01DB8JH4M/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B01DB8JH4M/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)

For instructions on how to connect the sensor to your pi, please refer to this tutorial: [https://www.instructables.com/id/RPIHomeBridge-TemperatureHumidity-Sensor/](https://www.instructables.com/id/RPIHomeBridge-TemperatureHumidity-Sensor/)

You do not need the extra resistor if you buy the prepared sensor from the Amazon link above as it already comes with a PCB that contains it.

## Preps

Install your Pi using [Noobs](https://www.raspberrypi.org/downloads/noobs/) and the Small Raspbian Image w/o desktop environment. You just need the pure console and some additional modules.

Config your system with

```plaintext
sudo raspi-config
```

Configure at least a hostname, ssh server and wifi settings.

Update your system and install additional modules

```plaintext
sudo apt-get update && apt-get upgrade -y
```

```plaintext
sudo apt-get install pigpio python-pigpio python3-pigpio -y
```

For Pi Zero install

```plaintext
sudo apt install pigpio python3-gpiozero python3-pigpio
```

To use the GPIO port you have to run a task at bootup.

```plaintext
sudo cp /usr/bin/pigpiod /etc/init.d
sudo systemctl enable pigpiod
```

I experienced problems enabling the service on a Pi Zero W. I got this error:

```plaintext
update-rc.d: error: pigpiod Default-Start contains no runlevels, aborting.
```

If this happens to you just open `/etc/rc.local` and add this line *before* the line `exit 0`.

```plaintext
pigpiod
```

This will execute the program at boot.

## Other machine settings (optional)

Set up a real root user.

```plaintext
sudo passwd
```

Also, you may want to set ssh access for the root user. Open the ```/etc/ssh/sshd_config``` file and search for the line

```plaintext
#PermitRootLogin prohibit-password
```

Change this to

```plaintext
PermitRootLogin yes #prohibit-password
```

Reboot the Pi or restart ssh with

```plaintext
sudo /etc/init.d/ssh restart
```


### Raspberry Pi 2/3 B (+)

Add Node.js to the package list

```plaintext
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
```

Install Node.js and other packages. We need ffmeg only if we want to use a Pi camera. The installation may take a while.

```plaintext
sudo apt install nodejs node-semver -y
```

### Raspberry Pi Zero W

For the Pi Zero W you cannot use predefined packages. Just download the dist package for ARMv6 architecture from [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

Then unpack the archive with

```plaintext
tar xvf <yourNodeFile.tar>
```

Copy all the files to your binary folder (it's in the $PATH automatically)

```plaintext
cp -r node.../* /usr/local
```

Check if node is found with

```plaintext
node -v
```

## Install the BCM2835 library

Grab the lib from here: [http://www.airspayce.com/mikem/bcm2835/](http://www.airspayce.com/mikem/bcm2835/)

```
# download the latest version of the library, say bcm2835-1.xx.tar.gz, then:

tar zxvf bcm2835-1.xx.tar.gz
cd bcm2835-1.xx
./configure
make
sudo make check
sudo make install
```

## Install Node.js packages

This will install all relevant node modules. This will take some time, please be patient.
There also will be a lot of message outputs but unless you run into an error you can ignore them all.

```plaintext
npm install -g --unsafe-perm pm2 homebridge homebridge-config-ui-x homebridge-dht
```

## Config file

My config file is located in ```/root/.homebridge/config.json```

If you don't have that folder and file yet, create them (all commands as root user)

```plaintext
cd
mkdir .homebridge
nano .homebridge/config.json
```

And this is the content in my case. You may want to alter it depending on the plugins and devices you are using.

```javascript
{
    "bridge": {
        "name": "Bridge_DHT",
        "username": "CC:22:3D:E3:CE:31",
        "port": 51826,
        "pin": "656-92-988"
    },
    "description": "I am a thermometer",
    "platforms": [
        {
            "platform": "config",
            "name": "Config",
            "port": 8083,
            "log": "/var/log/homebridge.stdout.log",
            "error_log": "/var/log/homebridge.stderr.log",
            "restart": "/usr/local/bin/supervisorctl restart homebridge"
        }
    ],
    "accessories": [
        {
            "accessory": "Dht",
            "name": "dht22",
            "name_temperature": "Temperature",
            "name_humidity": "Humidity",
            "service": "dht22"
        }
    ]
}
```

## Test installation

```plaintext
homebridge
```

Please notice: the Homebridge config UI takes a while to start. Give it at least 1 minute, then try to access it with your browser under `http://hostname:8083`

If no errors occur then congrats! You can stop the script with ```CTRL+C```

If you get errors then please Google them ;-)

## Add homebridge as a service at boot

I am using pm2 to startup node scripts at boot time

```plaintext
pm2 start homebridge
```

Alternatively you can start homebridge in the so called "insecure" mode which allows you to control the devices over the web UI. I don't know what's insecure about that, but hey, you can decide. In this case the command is

```plaintext
pm2 start "homebridge -I"
```

To list the processes that pm2 is running type

```plaintext
pm2 ls
```

To save the pm2 config type

```plaintext
pm2 save
```

To add pm2 to the start up procedure type

```plaintext
pm2 startup
```

## Reboot and check

After reboot check if the server is running by opening ```http://hostname:8083```. This will bring up the config UI. To login use "admin" as username and password. You can change the password later in the UI.

## Troubleshooting

Whenever you experiment with adding and removing the homebridge to your Homekit system, it may occur that you cannot re-add the homebridge after you removed (for testing). In this case change the username property in the config file.

In my case I could successfully re-add the bridge after changing this line:

```plaintext
"username": "CC:22:3D:E3:CE:31"
```

to

```plaintext
"username": "CC:22:3D:E3:CE:32"
```

in the config.json file. It seems that homekit chaches devices for a while so this is a roundtrip.

### Resources used

[https://www.instructables.com/id/RPIHomeBridge-TemperatureHumidity-Sensor/](https://www.instructables.com/id/RPIHomeBridge-TemperatureHumidity-Sensor/)

[https://www.npmjs.com/package/homebridge-dht](https://www.npmjs.com/package/homebridge-dht)

[https://github.com/NorthernMan54/homebridge-dht/blob/HEAD/Build.md](https://github.com/NorthernMan54/homebridge-dht/blob/HEAD/Build.md)

[https://www.raspberrypi.org/forums/viewtopic.php?p=1166851](https://www.raspberrypi.org/forums/viewtopic.php?p=1166851)

[https://www.raspberrypi.org/downloads/noobs/](https://www.raspberrypi.org/downloads/noobs/)

[https://www.raspberrypi.org/documentation/usage/gpio/](https://www.raspberrypi.org/documentation/usage/gpio/)

[https://github.com/nodejs/node-gyp/issues/809](https://github.com/nodejs/node-gyp/issues/809)